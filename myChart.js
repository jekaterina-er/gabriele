    Chart.defaults.global.defaultFontColor = 'black';
    Chart.defaults.global.defaultFontSize = 12;
    Chart.defaults.global.defaultFontFamily = "'Karla', sans-serif";

    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ['Unknown Species', 'Known Species'],
            datasets: [{
                label: 'Percentage of Fungi Species',
                data: [90, 10],
                backgroundColor: [
                'rgba(155, 158, 226, 1)',
                'rgba(63, 68, 200, 1)',
                ],
            borderWidth: 1,
            borderColor: ['rgba(255, 255, 255, 1)'],
            }]
        },
    });

    var ctx = document.getElementById("myPieChart").getContext('2d');
    var myPieChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ['Inedible', 'Edible', 'Sickly', 'Tasty','Poisonous' ],
            datasets: [{
                label: 'Percentage of Fungi Species',
                data: [50, 25, 40, 4, 1],
                backgroundColor: [
                'rgba(63, 68, 200, 1)',
                'rgba(93, 98, 209, 1)',
                'rgba(124, 127, 217, 1)',
                'rgba(154,157,226, 1)',
                'rgba(185,187,235, 1)',
                ],
            borderWidth: 1,
            borderColor: ['rgba(255, 255, 255, 1)'],
            }]
        },
    });